<!DOCTYPE html>
<html>
<head>
	<title>Pajx1</title>
</head>
<body>
	<h1>My Site1</h1>
  	<div class="container" id="pjax-container">
    	Go to <a href="/page2">next page</a>.
  	</div>
  	<script src="js/jquery.min.js"></script>
  	<script type="text/javascript" src="/js/jquery.pjax.js"></script>
  	<script type="text/javascript">
  		$(document).pjax('a', '#pjax-container')
  	</script>
</body>
</html>