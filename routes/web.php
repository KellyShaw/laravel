<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/page1', function() {
	return view('pajx1');
});

Route::get('/page2', function() {
	return view('pajx2');
});

Route::get('emoji', function() {
	$post = App\Post::first();
	return $post->content;
});